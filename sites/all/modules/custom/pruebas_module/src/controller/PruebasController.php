<?php
namespace Drupal\pruebas_module\Controller;
use Drupal\Core\Controller\ControllerBase;

class PruebasController extends ControllerBase {

  /* Método acción content devuelve directamente un contenido en html,
  este método será usado en una ruta */
  public function content() {

    //var_dump($_POST);

    return array(
      '#theme' => 'show_users',
      '#titulo' => $this->t('Listado de usuarios')
    );

  }

}

?>
