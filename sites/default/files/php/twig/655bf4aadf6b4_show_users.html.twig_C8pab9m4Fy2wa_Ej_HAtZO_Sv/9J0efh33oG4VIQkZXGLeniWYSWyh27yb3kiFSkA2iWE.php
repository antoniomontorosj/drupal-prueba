<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* sites/all/modules/custom/pruebas_module/templates/show_users.html.twig */
class __TwigTemplate_6024c0d71f5d717fc040e1aa45f33ad468a577fed3313f9ad8122da2099c6701 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = [];
        $filters = ["escape" => 3];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                [],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "

<h2>";
        // line 3
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["titulo"] ?? null)), "html", null, true);
        echo "</h2>
<div>
  <form method=\"post\" name=\"form\" id=\"form\" action=\"#\">
    <label for=\"search\">Buscar</label>
    <input type=\"text\" name=\"search\" id=\"search\">
    <div style=\"display: inline-flex\">
    <input type=\"radio\" name=\"tipo\" id=\"nombre\" value=\"nombre\">
    <label for=\"nombre\">Nombre</label>
    <input type=\"radio\" name=\"tipo\" id=\"apellidos\" value=\"apellidos\">
    <label for=\"apellidos\">Apellidos</label>
    <input type=\"radio\" name=\"tipo\" id=\"email\" value=\"email\">
    <label for=\"email\">Email</label>
    </div>
    <input type=\"submit\" name=\"enviar\" id=\"enviar\" value=\"Enviar\">
  </form>
  <div id=\"result\">

  </div>
</div>
<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js\"></script>
<script>
  \$(document).ready(function () {
    \$(\"form\").submit(function (event) {
      var formData = {
        search: \$(\"#search\").val(),
        tipo: \$(\"input:radio[name=tipo]:checked\").val(),
      };

      table=\"<table><caption>My table</caption><thead><tr><th>nombre de usuario</th><th>nombre</th><th>apellido1</th><th>apellido2</th><th>correo electronico</th></thead>\";

      \$.ajax({
        type: \"POST\",
        url: \"prueba.php\",
        data: formData,
        dataType: \"json\",
        encode: true,
      }).done(function (data) {
        console.log(data);
        console.log(data.usuarios)
        console.log(data.usuarios.length)
        for(let i = 0; i < data.usuarios.length; i++) {
          console.log(data.usuarios[i].email);  // (o el campo que necesites)
          table += \"<tr><td>\"+data.usuarios[i].name+\" \"+data.usuarios[i].surname1+\" \"+data.usuarios[i].surname2+\"</td>\";
          table += \"<td>\"+data.usuarios[i].name+\"</td>\";
          table += \"<td>\"+data.usuarios[i].surname1+\"</td>\";
          table += \"<td>\"+data.usuarios[i].surname2+\"</td>\";
          table += \"<td>\"+data.usuarios[i].email+\"</td>\";
          table += \"</tr>\";

        }
        table += \"</table>\";
        console.log(table);
        \$(\"#result\").html(table);
      });

      event.preventDefault();
    });
  });
</script>
";
    }

    public function getTemplateName()
    {
        return "sites/all/modules/custom/pruebas_module/templates/show_users.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 3,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "sites/all/modules/custom/pruebas_module/templates/show_users.html.twig", "C:\\xampp2\\htdocs\\drupal-8.9.20\\sites\\all\\modules\\custom\\pruebas_module\\templates\\show_users.html.twig");
    }
}
